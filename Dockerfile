from debian:bullseye-slim

RUN apt-get update \
	&& apt-get -y install \
		python3-pip \
	&& pip install qmk
RUN apt-get -y install \
	avr-libc \
	avrdude \
	dfu-programmer \
	dfu-util \
	gcc-arm-none-eabi \
	gcc-avr \
	git
